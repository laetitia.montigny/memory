/* Etape 1 */
const HIDDEN = 0;
const RETURNED = 1;
const FOUND = 2;
const PATH = "images/";
const BACK = "back.jpg";
var tCards = ["a", "a", "b", "b", "c", "c", "d", "d", "e", "e", "f", "f"];
var nbTestedCards = 0;
var tCardsPlayset = [
  HIDDEN,
  HIDDEN,
  HIDDEN,
  HIDDEN,
  HIDDEN,
  HIDDEN,
  HIDDEN,
  HIDDEN,
  HIDDEN,
  HIDDEN,
  HIDDEN,
  HIDDEN,
];
var errors = 0;
var bestScore = 1000;

/**Fonctions Niveau 1*/
function initCards() {
  tCards = tCards.sort(function () {
    return 0.5 - Math.random();
  });
  for (i in tCards) {
    var cardName = tCards[i];
    var card = createCard(i, cardName );
    document.getElementById("playset").appendChild(card);
  }
}

/* fonctions Niveau 2*/

function createCard(idCard, cardName) {
  var cardImg = document.createElement("img");
  cardImg.setAttribute("src", PATH + BACK);
  cardImg.setAttribute("id", idCard);
  cardImg.setAttribute("name", cardName);
  cardImg.setAttribute("class", "col-xs-3");
  cardImg.setAttribute("draggable", false);
  cardImg.setAttribute("alt", "card");
  cardImg.setAttribute("onClick", "flipCard(" + idCard + ");");
  return cardImg;
}
/* Fonctions Niveau 3 */

function flipCard(idCard) {
  if (tCardsPlayset[idCard] == HIDDEN) {
    if (nbTestedCards < 2) {
      document.getElementById(idCard).src =
        PATH + document.getElementById(idCard).name + ".jpg";
      tCardsPlayset[idCard] = RETURNED;
      nbTestedCards++;
      // Ajout du timer si 2eme carte testée
      if (nbTestedCards == 2) {
        if (!testCouple()) {
          setTimeout("hideTestedCards()", 1000);
        }
      }
    }
  }
}
/* Fonctions Niveau 4 */
function hideTestedCards() {
  for (var i in tCardsPlayset) {
    if (tCardsPlayset[i] == RETURNED) {
      tCardsPlayset[i] = HIDDEN;
      document.getElementById(i).src = PATH + BACK;
    }
  }
  nbTestedCards = 0;
  //affiche les erreurs
  errors++;
  document.getElementById("errors").textContent = "Errors: " + errors;
}

function testCouple() {
  var nameFirstImg = "";
  var idFirstCardTested;

  for (var id in tCardsPlayset) {
    if (tCardsPlayset[id] == RETURNED) {
      var image = document.getElementById(id);
      if (nameFirstImg == image.name) {
        // Second passage avec cartes qui matchent
        nbTestedCards = 0;
        tCardsPlayset[id] = FOUND;
        tCardsPlayset[idFirstCardTested] = FOUND;
        testBestScore();
        return true; // Couple OK
      } else {
        //Premier passage, initialise la 1ere carte retournée
        nameFirstImg = image.name;
        idFirstCardTested = id;
      }
    }
  }
  return false;
}
/*Fonction Niveau 5*/
function testBestScore() {
  for (var id in tCardsPlayset) {
    if (tCardsPlayset[id] !== FOUND) {
      return false;
    }
  }
  if (bestScore > errors) {
    bestScore = errors;
    document.getElementById("congratulations").textContent =
      "CONGRATULATIONS!!!!";
  }
}

/****************** PROGRAMME PRINCIPAL ********************/
initCards();
console.log("DEBUG:OK"); // CONTROLE DEBUG

/****************** NOUVELLE PARTIE ********************/
function newGame() {
  if (bestScore != 1000) {
    document.getElementById("bestScore").textContent =
      "Only " + bestScore + " errors";
  }

  document.getElementById("congratulations").outerHTML =
    '<p id="congratulations"></p>';
  document.getElementById("playset").outerHTML = ' <div id="playset"></div>';
  // tCards = ["a", "a", "b", "b", "c", "c", "d", "d", "e", "e", "f", "f"];
  nbTestedCards = 0;
  tCardsPlayset = [
    HIDDEN,
    HIDDEN,
    HIDDEN,
    HIDDEN,
    HIDDEN,
    HIDDEN,
    HIDDEN,
    HIDDEN,
    HIDDEN,
    HIDDEN,
    HIDDEN,
    HIDDEN,
  ];

  errors = 0;
  document.getElementById("errors").textContent = "Errors: " + errors;
  initCards();
}
